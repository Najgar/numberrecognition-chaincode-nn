function [ feature_vector,block_size ] = read_training_file( )
%READ_TRAINING_FILE Summary of this function goes here
%   Detailed explanation goes here

fileID = fopen('training.txt','r');
data = {};
tline = fgetl(fileID);
while ischar(tline)
    data{length(data)+1} = tline;
    tline = fgetl(fileID);
end
fclose(fileID);
block_size = str2num(data{1});
index = 1;
t = 1;
s = 1;
feature_vector = {};
feature_vector{index} = {};
for i = 1:block_size
    feature_vector{index}{i} = {};
    for j = 1:block_size
        feature_vector{index}{i}{j} = {};
    end
end
for i = 2:length(data)
    if (i > 2)
        s = s + 1;
        if(s > block_size)
            t = t + 1;
            s = 1;
        end
        if (t > block_size)
            t = 1;
        end
    end
    handler = '';
    s1 = '';
    s2 = '';
    for j = 1:length(data{i})
        if data{i}(j) == '#'
            index = index + 1;
            if i ~=  length(data)
                feature_vector{index} = {};
                for i = 1:block_size
                    feature_vector{index}{i} = {};
                    for j = 1:block_size
                        feature_vector{index}{i}{j} = {};
                    end
                end
            end
        elseif data{i}(j) == ','
            feature_vector{index}{t}{s}{length(feature_vector{index}{t}{s}) + 1} = str2num(handler);
            handler = '';
        elseif j == length(data{i})
            feature_vector{index}{t}{s}{length(feature_vector{index}{t}{s}) + 1} = str2num(handler);
        else
            handler = strcat(handler , data{i}(j));
        end    
    end
end
end

