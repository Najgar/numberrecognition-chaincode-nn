function [ z ] = set_matrix( feature_vector , block_size )
%SET_MATRIX Summary of this function goes here
%   Detailed explanation goes here
z = ones(100,(block_size*block_size*8)+1);
colcounter = 1;

for i=1:100
    colcounter = 1;
    for j=1:length(feature_vector{i})
       for k=1:length(feature_vector{i}{j})
           for l=1:8
               z(i,colcounter) = feature_vector{i}{j}{k}{l};
               colcounter = colcounter + 1;
           end
       end
    end
end

end

