function [ w ] = getweights( z )
%GETWEIGHTS Summary of this function goes here
%   Detailed explanation goes here
    %z = z + 1;
    %z(z==0) = 1;
    z = z * -1;
    [r c] = size(z);
    b = ones(100,1)
    w = ones(10,c);
    for i=1:10
        z(((i-1)*10)+1:i*10,:) = z(((i-1)*10)+1:i*10,:)*-1;
        res = transpose(pinv(transpose(z)*z)*transpose(z)*b);
        for j=1:length(res)
            w(i,j) = res(j);
        end
        z(((i-1)*10)+1:i*10,:) = z(((i-1)*10)+1:i*10,:)*-1;
    end
end

