function [ R ] = get_radius( b,cx,cy )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    R = 0;
    for i=1:length(b)
        x = b(i,1);
        y = b(i,2);
        
        temp = (cx - x)^2;
        temp2 = (cy - y)^2;
        
        res = sqrt(temp + temp2);
        
        if (res > R)
            R = res;
        end
    end
    
end

