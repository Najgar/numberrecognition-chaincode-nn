function [A] = image_trim( A )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

    img = A;
    [row,col] = find(img);
    max_height = max(row);
    min_height = min(row);
    max_width = max(col);
    min_width = min(col);

    new_image = img(min_height:max_height,min_width:max_width);
    A = new_image;
end

