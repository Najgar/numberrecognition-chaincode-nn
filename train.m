function [ net ] = train( )
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here

prompt = 'How many block to train data with : ';
block_size = input(prompt);
A = image_read(block_size);
feature_vector = {};
for i= 1:length(A)
    feature_vector{i} = get_feature_vector(A{i}{2}.img , block_size);
end
z = set_matrix(feature_vector,block_size);
[r c] = size(z)
matrix(1:100,1:10)=0;
for i=1:10
    matrix(((i-1)*10)+1:i*10,i:i) = matrix(((i-1)*10)+1:i*10,i:i) + 1;
end

% net = newff(minmax(z),[12 10],{'tansig','tansig','tansig'},'trainlm');
net=feedforwardnet([12 10]);
net.trainParam.epochs=50;
net.trainParam.goal = 0.01;
net.layers{1}.transferFcn = 'logsig';
net.layers{2}.transferFcn = 'logsig';
net = train(net,z',matrix');
end

