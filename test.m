function [ response ] = test( net )
%TEST Summary of this function goes here
%   Detailed explanation goes here

block_size = 4;


img = imread('testimg.bmp');
img = image_inverse(img);
img = image_trim(img);
img = image_adjust(img,block_size);

test_file_feature_vector = get_feature_vector(img , block_size);

f = ones(1,length(test_file_feature_vector));
colcounter = 1;
for j=1:length(test_file_feature_vector)
   for k=1:length(test_file_feature_vector{j})
       for l=1:8
           f(1,colcounter) = test_file_feature_vector{j}{k}{l};
           colcounter = colcounter + 1;
       end
   end
end

response = sim(net,f)
end

